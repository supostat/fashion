<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PostController@index');

Route::resource('post', 'PostController');

Route::get('getTile',['as' => 'getTile', 'uses' => 'PostController@addTile']);
Route::get('addSliderImage',['as' => 'addSliderImage', 'uses' => 'PostController@addSliderImage']);
Route::delete('removeTile',['as' => 'removeTile', 'uses' => 'PostController@removeTile']);