<?php

namespace App\Http\Controllers;

use App\Fashion\TileModel;
use App\Post;
use App\Post_tag;
use App\Post_tile;
use App\Tag;
use App\Tile;
use App\Type;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        $tiles = Tile::all();
        $post = new Post();
        return view('post.create', compact('post', 'types', 'tiles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post($request->all());
        $post->published = true;
        $post->save();

        if (isset($request->all()['tile_item'])) {
            $tile_items = $this->sortArrayByArray($request->all()['tile_item'], array_keys($request->all()['order']));
            $count = 0;
            foreach ($tile_items as $item) {
                $table = key($item);
                $tile_id = Tile::where('table', $table)->first()->id;
                $post_tile = new Post_tile;
                $post_tile->post_id = $post->id;
                $post_tile->tile_id = $tile_id;
                $post_tile->save();
                foreach ($item as $attributes) {
                    foreach ($attributes as $id => $attribute) {
                        if ($table == 'slideshow') {
                            $item_order = 0;
                            foreach ($attribute as $status => $images) {
                                foreach ($images as $image) {
                                    if ($image && ($image->isValid())) {
                                        $real_name = $image->getClientOriginalName();
                                        $hashed_name = uniqid() . '.' . $image->getClientOriginalExtension();
                                        try {
                                            $file = $image->move('images', $hashed_name);
                                        } catch (FileException $e) {
                                        }
                                    }
                                    $tile_item_model = new TileModel;
                                    $tile_item_model->setTile($table);
                                    $tile_item_model->post_id = $post->id;
                                    $tile_item_model->real_name = $real_name;
                                    $tile_item_model->hashed_name = $hashed_name;
                                    $tile_item_model->type_id = $id;
                                    $tile_item_model->order = $count;
                                    $tile_item_model->item_order = $item_order;
                                    $tile_item_model->save();
                                    $item_order ++;
                                }
                            }
                        } else {
                            $tile_item_model = new TileModel;
                            $tile_item_model->setTile($table);
                            $tile_item_model->post_id = $post->id;
                            $tile_item_model->value = $attribute;
                            $tile_item_model->type_id = $id;
                            $tile_item_model->order = $count;
                            $tile_item_model->save();
                        }
                    }
                }
                $count++;
            }
        }
        return Redirect::route('post.index')->with([
            'flash_message' => 'Post created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $types = Type::all();
        $tiles = Tile::all();

        return view('post.edit', compact('post', 'types', 'tiles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image_ids = [];
        foreach ($request->all()['tile_item'] as $items) {
            foreach ($items as $item) {
                foreach ($item as $ids) {
                    if (!isset($image_ids[key($items)])) {
                        $image_ids[key($items)] = [];
                    }
                    if (isset($ids['old'])) {
                        $image_ids[key($items)] = array_merge($image_ids[key($items)], array_keys($ids['old']));
                    }
                }
            }
        }
        foreach ($image_ids as $table_name => $ids) {
            TileModel::tile($table_name)->where('post_id', $id)->whereNotIn('id', $ids)->delete();
        }

        $post = Post::find($id);
        $post->fill($request->all());
        $post->save();

        Post_tag::where('post_id', $id)->delete();

        $this->addTags($request, $id);
        foreach ($post->tiles as $tile) {
            if($tile->table != 'slideshow') {
                TileModel::tile($tile->table)->where('post_id', $post->id)->delete();
            }
        }
        Post_tile::where('post_id', $post->id)->delete();
        if (isset($request->all()['tile_item'])) {
            $tile_items = $this->sortArrayByArray($request->all()['tile_item'], array_keys($request->all()['order']));
            $count = 0;
            foreach ($tile_items as $order => $item) {
                $table = key($item);
                $tile_id = Tile::where('table', $table)->first()->id;
                $post_tile = new Post_tile;
                $post_tile->post_id = $post->id;
                $post_tile->tile_id = $tile_id;
                $post_tile->save();
                foreach ($item as $attributes) {
                    foreach ($attributes as $id => $attribute) {
                        if ($table == 'slideshow') {
                            $image_order = 0;
                            foreach ($attribute as $status => $images) {
                                foreach ($images as $image_id => $image) {
                                    if ($status == 'old') {
                                        $tile_item_model = TileModel::tile('slideshow')->where('post_id', $post->id)->where('id', $image_id)->first();
                                        $tile_item_model->setTable($table . '__tiles');
                                    } else {
                                        $tile_item_model = new TileModel;
                                        $tile_item_model->setTile($table);
                                        $tile_item_model->post_id = $post->id;
                                    }
                                    if ($image && ($image->isValid())) {
                                        $real_name = $image->getClientOriginalName();
                                        $hashed_name = uniqid() . '.' . $image->getClientOriginalExtension();
                                        try {
                                            $file = $image->move('images', $hashed_name);
                                        } catch (FileException $e) {
                                        }
                                        $tile_item_model->real_name = $real_name;
                                        $tile_item_model->hashed_name = $hashed_name;
                                        $tile_item_model->type_id = $id;
                                    }
                                    $tile_item_model->order = $count;
                                    $tile_item_model->item_order = $image_order;
                                    $image_order ++;
                                    $tile_item_model->save();
                                }

                            }
                        } else {
                            $tile_item_model = new TileModel;
                            $tile_item_model->setTile($table);
                            $tile_item_model->post_id = $post->id;
                            $tile_item_model->value = $attribute;
                            $tile_item_model->type_id = $id;
                            $tile_item_model->order = $count;
                            $tile_item_model->save();
                        }
                    }
                }
                $count++;
            }
        }
        return Redirect::route('post.index')->with([
            'flash_message' => 'Post updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);

        return Redirect::route('post.index')->with([
            'flash_message' => 'Post deleted'
        ]);
    }

    public function addSliderImage(Request $request) {
        if($request->ajax()) {
            $tile_id = $request->tile_id;
            $tile = Tile::find($tile_id);
            $post_id = $request->post_id;
            $order = $request->order;
            $html = [];
            foreach ($tile->attribs as $item) {
                $html[] = $item->render($tile->table, $item->type, $item->id, $order, true);
            }
            echo implode($html);
        }
    }
    public function addTile(Request $request)
    {
        if ($request->ajax()) {
            $tile_id = $request->id;
            $tile = Tile::find($tile_id);
            $post_id = $request->post_id;
            $tile_order = $request->tile_order;
            $html = [];
            $html[] = '<div class="tile-box" data-post-id="' . $post_id . '" data-tile-id="' . $tile_id . '" data-order="'. $tile_order .'">';
            $html[] = '<div class="icons"><div class="delete"><span class="glyphicon glyphicon-trash"></span></div><div class="drag-tile"><span class="glyphicon glyphicon-sort"></span></div></div>';
            $html[] = '<div class="header">';
            $html[] = $tile->name;
            $html[] = '</div>';
            if ($tile->table == 'slideshow') {
                $html[] = '<div class="slider-images" id="si' . $tile_order . '">';
            }
            foreach ($tile->attribs as $attribute) {
                $html[] = $attribute->render($tile->table, $attribute->type, $attribute->id, $tile_order, true);
            }
            if ($tile->table == 'slideshow') {
                $html[] = '</div>';
                $html[] = '<div class="add-slider-image glyphicon glyphicon-plus"></div>';
            }
            $html[] = '</div>';

            return implode($html);
        }
    }

    public function removeTile(Request $request)
    {
        if ($request->ajax()) {
            $tile_id = $request->tile_id;
            $tile = Tile::find($tile_id);
            $post_id = $request->post_id;
            if ($post_id) {
                try {
                    TileModel::tile($tile->table)->where('post_id', $post_id)->delete();
                    Post_tile::where('post_id', $post_id)->where('tile_id', $tile_id)->delete();
                    echo $tile->name;
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }
            }
        }
    }

    protected function sortArrayByArray(Array $array, Array $orderArray) {
        $ordered = array();
        foreach($orderArray as $key) {
            if(array_key_exists($key,$array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
            }
        }
        return $ordered + $array;
    }

    protected function addTags(Request $request, $post_id)
    {
        if ($request->tags) {
            $tags = explode(',', $request->tags);
            foreach ($tags as $tag) {
                $tag_model = Tag::firstOrCreate(['name' => $tag]);
                Post_tag::create(['post_id' => $post_id, 'tag_id' => $tag_model->id]);
            }
        }
    }
}
