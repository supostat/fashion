<?php
/**
 * Created by PhpStorm.
 * User: PugachevIgor
 * Date: 10.10.2015
 * Time: 11:16
 */
namespace App\Fashion;

use Illuminate\Database\Eloquent\Model;

class TileModel extends Model
{

    protected $name;
    protected $tile_table;
    protected $tile_order;
    public $timestamps = false;

    public function render($name, $type, $id = null, $tile_order = null, $new = false)
    {
        $this->name = $name;
        $this->type_id = $id;
        $this->new = $new;
        $this->tile_order = $tile_order;
        return $this->{'HTML_' . $type}();
    }

    private function HTML_input()
    {
        $html = [];
        $html[] = '<div class="form-group">';
        $html[] = '<input name="tile_item['. $this->tile_order .'][' . $this->name . ']['. $this->type_id .']" value="' . $this->value . '" class="form-control">';
        $html[] = '<input type="hidden" name="order['. $this->tile_order .']">';
        $html[] = '</div>';
        return implode($html);
    }

    private function HTML_text()
    {
        $html = [];
        $html[] = '<div class="form-group">';
        $html[] = '<textarea name="tile_item['. $this->tile_order .'][' . $this->name . ']['. $this->type_id .']" class="form-control">' . $this->value . '</textarea>';
        $html[] = '<input type="hidden" name="order['. $this->tile_order .']">';
        $html[] = '</div>';
        return implode($html);
    }

    private function HTML_images()
    {
        $html = [];
        $html[] = '<div class="form-group slider-image">';
        $html[] = '<span class="select-image glyphicon glyphicon-picture"></span>';
        $html[] = '<input type="file" accept="image/jpeg,image/png" placeholder="Select an image" name="tile_item['. $this->tile_order .'][' . $this->name . ']['. $this->type_id .']'. ($this->new ? '[new][]' : '[old]['.$this->id.']') .'" class="form-control" value="'. $this->real_name .'">';
        $html[] = '<span class="filename">'. $this->real_name .'</span></span><span class="delete-image glyphicon glyphicon-minus"></span><span class="drag-image glyphicon glyphicon-sort"></span>';

        $html[] = '<input type="hidden" name="order['. $this->tile_order .']">';
        $html[] = '</div>';
        return implode($html);
    }

    public static function tile($tile_name, $type = null){
        $instance = new static;
        $instance->setTile($tile_name, $type);
        return $instance->newQuery();
    }

    public function setTile($tile_table, $type = null){
        $this->tile_table;
        if($tile_table != null){
            $this->table = $tile_table.'__tiles';
        }
    }

    public function newInstance($attributes = array(), $exists = false)
    {
        $model = parent::newInstance($attributes, $exists);
        $model->setTile($this->tile_table);
        return $model;
    }

    public function attribute()
    {
        return $this->belongsTo('App\Attribute', 'type_id');
    }
}