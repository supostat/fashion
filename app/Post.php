<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fashion\TileModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\QueryException;

class Post extends Model
{
    protected $fillable = ['name', 'sub_name', 'type_id'];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function tiles()
    {
        return $this->belongsToMany('App\Tile', 'post_tiles', 'post_id', 'tile_id');
    }

    public function attributes($class_name, $count) {
        $instance = new TileModel();
        $instance->setTile($class_name);
        try {
            $relation = new HasMany($instance->newQuery(), $this, $this->getForeignKey(), $this->getKeyName());
            $result = $relation->get()->filter(function ($item) use ($count) {
                if($item->order == $count) {
                    return true;
                }
                return false;
            });
            if($class_name == 'slideshow') {
                return $result->sortBy('item_order');
            }
            return $result;
        } catch(QueryException $e) {
            return [];
        }
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'post_tags');
    }

    public function sliders()
    {
        $instance = new TileModel();
        $instance->setTile('slideshow');
        try {
            $relation = new HasMany($instance->newQuery(), $this, $this->getForeignKey(), $this->getKeyName());
            return $relation;
        } catch(QueryException $e) {
            return [];
        }
    }
}
