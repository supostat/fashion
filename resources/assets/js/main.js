/**
 * Created by PugachevIgor on 07.10.2015.
 */

(function ($) {
    var tiles_array = [];
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#type_id').selectize();

        var tiles_list = $('#tiles_list').selectize({
            sortField: "value"
        });

        $('input#tags').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            create: function(input) {
                return {
                    value: input,
                    text: input
                }
            }
        });

        $('.slide-block .title').on('click', function () {
            $(this).parent().find('.content').slideToggle();
            $(this).parent().toggleClass('active');
        });

        $('#add-tile').on('click', function () {
            var tiles = $('#tiles_list'),
                post_id = $('#post').data('id'),
                tile_id = tiles.val();
            // tiles_list[0].selectize.removeOption(tile_id);
            if(!tiles_array[tile_id]) {
                tiles_array[tile_id] = 0;
            }
            tiles_array[tile_id] ++;

            console.log(tiles_array);
            $.ajax('/getTile', {
                method:'GET',
                data: {
                    id : tile_id,
                    post_id: post_id,
                    tile_number: tiles_array[tile_id]
                },
                success: function (res) {
                    $('#tiles').append(res);
                }
            });
        });

        $(document).on('click', '.icons .delete', function () {
            var tile_box = $(this).parents('.tile-box'),
                tile_id = tile_box.data('tile-id'),
                post_id = $('#post').data('id');

            tiles_array[tile_id] --;
            if(!tiles_array[tile_id]) {
                delete(tiles_array[tile_id]);
            }
            console.log(tiles_array);
            tile_box.remove();
            //$.ajax('/removeTile', {
            //    method: 'delete',
            //    data: {
            //        tile_id: tile_id,
            //        post_id: post_id
            //    },
            //    success: function (res) {
            //        tiles_list[0].selectize.addOption({value: tile_id, text: res});
            //        tiles_list[0].selectize.refreshItems();
            //        tile_box.remove();
            //    }
            //});
        });
    });
})(jQuery);

