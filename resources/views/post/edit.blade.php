@extends('welcome')

@section('content')
    <?php $count=0; ?>
    <div class="row" id="post" data-id="{{ $post->id }}">
        <div class="col-md-4">
            <h1 class="page-header">Edit Post</h1>
            {!! Form::open(['method' => 'PUT', 'route' => ['post.update', $post->id], 'files'=> true]) !!}
            @include('post.partials.form')
            <section id="tiles">
                @foreach($post->tiles()->orderBy('post_tiles.id', 'asc')->get() as $id => $tile)
                    <div class="tile-box" data-tile-id="{{ $tile->id }}" data-post-id="{{ $post->id }}" data-order="{{ $count }}">
                        <div class="icons">
                            <div class="delete"><span class="glyphicon glyphicon-trash"></span></div>
                            <div class="drag-tile"><span class="glyphicon glyphicon-sort"></span></div>
                        </div>
                        <div class="header">{{ $tile->name }}</div>
                        @if($tile->table == 'slideshow')
                            <div class="slider-images" id="si{{ $count }}">
                        @endif
                        @foreach($post->attributes($tile->table, $count) as $item)
                            {!! $item->render($tile->table, $item->attribute->type, $item->attribute->id, $item->order) !!}
                        @endforeach
                        @if($tile->table == 'slideshow')
                            </div>
                            <div class="add-slider-image glyphicon glyphicon-plus"></div>
                        @endif
                    </div>
                    <?php
                    $count ++;
                    ?>
                @endforeach
            </section>
            {!! Form::close() !!}
        </div>
        <div class="col-md-8"></div>
    </div>
@endsection