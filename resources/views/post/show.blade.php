@extends('welcome')

@section('content')
    <div class="arrow-left"></div>
    <h1>{{ $post->name }}</h1>
        @foreach($post->sliders->groupBy('order') as $slider)
            <div class="slider" style="width: 300px;margin: 70px;display: inline-block;">
            @foreach($slider as $item)
                <div>
                    <img src="{{ Image::url(asset('images/'.$item->hashed_name),300,300,array('crop','grayscale')) }}"
                         alt="">
                </div>
            @endforeach
            </div>
        @endforeach
@stop