@extends('welcome')

@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <a href="{{ route('post.create') }}" class="btn btn-primary">New page</a>
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Type</th>
                <th>Published</th>
                <th>Publish at</th>
                <th>Author</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
            <tr>
                <td><a href="{{ route('post.edit', ['id' => $post->id]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td><a href="{{ route('post.show', $post->id) }}">{{ $post->name }}</a></td>
                <td>{{ $post->type->name }}</td>
                <td>
                    @if ($post->published)
                        YES
                    @else
                        NO
                    @endif
                </td>
                <td>
                    {{ $post->created_at }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection