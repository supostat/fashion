<a style="margin-bottom: 20px;display: block;" href="{{ route('post.index') }}"><< Back</a>
<div class="form-group">
    <button class="btn btn-primary">Save</button>
    @if($post->id)
        <a href="/post/{{ $post->id }}" data-method="delete" data-token="{{csrf_token()}}" class="btn btn-danger">Delete</a>
    @endif
</div>
<div class="form-group">
    <input type="text" class="form-control" name="name" id="name" required placeholder="Name" value="{{ $post->name }}" >
</div>
<div class="form-group">
    <input type="text" class="form-control" name="sub_name" id="sub-name" placeholder="Sub title" value="{{ $post->sub_name }}">
</div>
<div class="form-group">
    <select class="form-control" name="type_id" id="type_id">
        @foreach ($types as $type)
            @if($type->id == $post->type_id)
                <option selected value="{{ $type->id }}">{{ $type->name }}</option>
            @else
                <option value="{{ $type->id }}">{{ $type->name }}</option>
            @endif

        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="tags">Add tags</label>
    @if($post->tags)
        <input type="text" name="tags" value="{{ implode(',', array_flatten($post->tags()->get()->pluck('name'))) }}" id="tags">
    @else
        <input type="text" name="tags" id="tags">
    @endif
</div>
<section class="slide-block" id="slide-show">
    <div class="title">
        <span class="glyphicon arrow"></span>
        home slideshow</div>
    <div class="content">
        <h1>home slideshow</h1>
    </div>
</section>
<section class="slide-block" id="advanced">
    <div class="title">
        <span class="glyphicon arrow"></span>
        advanced</div>
    <div class="content">
        <h1>advanced</h1>
    </div>
</section>

<h2>Content</h2>
<div class="form-group form-inline">
    <select name="tile" id="tiles_list">
        <option value="">-- select a tile --</option>
        @foreach($tiles as $tile)
            <option value="{{ $tile->id }}">{{ $tile->name }}</option>
        @endforeach
    </select>
    <button id="add-tile" type="button" class="btn btn-primary">Add</button>
</div>
