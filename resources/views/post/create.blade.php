@extends('welcome')

@section('content')
    <div class="row" id="post">
        <div class="col-md-4">
            <h1 class="page-header">Create Post</h1>
            {!! Form::open(['route' => 'post.store', 'files'=> true]) !!}

            @include('post.partials.form')
            <section id="tiles">
            </section>
            {!! Form::close() !!}
        </div>
        <div class="col-md-8"></div>
    </div>

@endsection