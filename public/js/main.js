/**
 * Created by PugachevIgor on 07.10.2015.
 */

(function ($) {


    $(function () {
        laravel.initialize();
        var tiles_array = [];

        function getFileName(str, el) {
            var i;
            if (str.lastIndexOf('\\')) {
                i = str.lastIndexOf('\\') + 1;
            } else {
                i = str.lastIndexOf('/') + 1;
            }
            el.html(str.slice(i));
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var tiles_count = $('.tile-box').length;

        $('.slider-images').each(function (index, item) {
            var el = document.getElementById($(item).attr('id'));
            Sortable.create(el, {
                handle: '.drag-image',
                animation: 150
            });
        });

        $('#type_id').selectize();

        if($('#tiles').length) {
            Sortable.create(tiles, {
                handle: '.drag-tile',
                animation: 150
            });
        }

        $(document).on('change', 'input[type=file]', function () {
            var el = $(this).siblings('.filename');
            console.log(el);
            getFileName(this.value, el);
        });

        var tiles_list = $('#tiles_list').selectize({
            sortField: "value"
        });

        $(document).on('click', '.delete-image', function () {
            $(this).parent('.slider-image').remove();
        });

        $('input#tags').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });

        $('.slide-block .title').on('click', function () {
            $(this).parent().find('.content').slideToggle();
            $(this).parent().toggleClass('active');
        });

        $('.slider').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            nextArrow: '<div class="arrow-right"></div>',
            prevArrow: '<div class="arrow-left"></div>'
        });

        $(document).on('click', '.add-slider-image', function () {
            var slider = $(this).parent();

            var post_id = $('#post').data('id'),
                order = $(this).parent('.tile-box').data('order'),
                tile_id = $(this).parent('.tile-box').data('tile-id');

            $.ajax('/addSliderImage', {
                method: 'GET',
                data: {
                    post_id: post_id,
                    order: order,
                    tile_id: tile_id
                },
                success: function(res) {
                    $(res).appendTo(slider.find('.slider-images'));
                }
            });
            console.log(post_id);
            console.log(order);
        });

        $('#add-tile').on('click', function () {
            var tiles = $('#tiles_list'),
                post_id = $('#post').data('id'),
                tile_id = tiles.val();
            $.ajax('/getTile', {
                method: 'GET',
                data: {
                    id: tile_id,
                    post_id: post_id,
                    tile_order: tiles_count
                },
                success: function (res) {
                    $('#tiles').append(res);
                    if ($(res).find('.slider-images').length) {
                        var el = document.getElementById('si' + tiles_count);
                        Sortable.create(el, {
                            handle: '.drag-image',
                            animation: 150
                        });
                    } else {
                    }
                    tiles_count++;
                }
            });
        });

        $(document).on('click', '.icons .delete', function () {
            var tile_box = $(this).parents('.tile-box'),
                tile_id = tile_box.data('tile-id'),
                post_id = $('#post').data('id');

            tiles_array[tile_id]--;
            if (!tiles_array[tile_id]) {
                delete(tiles_array[tile_id]);
            }
            console.log(tiles_array);
            tile_box.remove();
        });
    });
})(jQuery);

